/*
Navicat MySQL Data Transfer

Source Server         : 本地连接
Source Server Version : 50536
Source Host           : localhost:3306
Source Database       : mycurdesign

Target Server Type    : MYSQL
Target Server Version : 50536
File Encoding         : 65001

Date: 2020-07-15 10:32:23
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `compressor`
-- ----------------------------
DROP TABLE IF EXISTS `compressor`;
CREATE TABLE `compressor` (
  `compressorId` int(11) NOT NULL AUTO_INCREMENT,
  `compressorState` int(11) DEFAULT NULL,
  PRIMARY KEY (`compressorId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of compressor
-- ----------------------------
INSERT INTO `compressor` VALUES ('1', '2');

-- ----------------------------
-- Table structure for `dataframe`
-- ----------------------------
DROP TABLE IF EXISTS `dataframe`;
CREATE TABLE `dataframe` (
  `dataFrameId` int(11) NOT NULL AUTO_INCREMENT,
  `deviceCoding` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `deviceAddr` int(11) DEFAULT NULL,
  `stateUpInterval` int(11) DEFAULT NULL,
  `startOutTime` int(11) DEFAULT NULL,
  `settingTem` varchar(11) COLLATE utf8_bin DEFAULT NULL,
  `TemCBias` int(11) DEFAULT NULL,
  `deviceState` int(11) DEFAULT NULL,
  `compreRunState` int(11) DEFAULT NULL,
  `percepTem` varchar(11) COLLATE utf8_bin DEFAULT NULL,
  `drawer1` int(11) DEFAULT NULL,
  `drawer2` int(11) DEFAULT NULL,
  `drawer3` int(11) DEFAULT NULL,
  `drawer4` int(11) DEFAULT NULL,
  `drawer5` int(11) DEFAULT NULL,
  `drawer6` int(11) DEFAULT NULL,
  `drawer7` int(11) DEFAULT NULL,
  `drawer8` int(11) DEFAULT NULL,
  `drawer9` int(11) DEFAULT NULL,
  `drawer10` int(11) DEFAULT NULL,
  PRIMARY KEY (`dataFrameId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of dataframe
-- ----------------------------
INSERT INTO `dataframe` VALUES ('1', '68736253954', '2', '2', '2', '20.0', '1', '0', '2', '21.0', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1');

-- ----------------------------
-- Table structure for `decoding`
-- ----------------------------
DROP TABLE IF EXISTS `decoding`;
CREATE TABLE `decoding` (
  `deviceCodingId` int(11) NOT NULL AUTO_INCREMENT,
  `deviceCoding` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `deviceAddress` int(11) DEFAULT NULL,
  PRIMARY KEY (`deviceCodingId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of decoding
-- ----------------------------
INSERT INTO `decoding` VALUES ('1', '68736253954', '2');

-- ----------------------------
-- Table structure for `drawers`
-- ----------------------------
DROP TABLE IF EXISTS `drawers`;
CREATE TABLE `drawers` (
  `seq` int(11) NOT NULL AUTO_INCREMENT,
  `drawer1` int(11) DEFAULT NULL,
  `drawer2` int(11) DEFAULT NULL,
  `drawer3` int(11) DEFAULT NULL,
  `drawer4` int(11) DEFAULT NULL,
  `drawer5` int(11) DEFAULT NULL,
  `drawer6` int(11) DEFAULT NULL,
  `drawer7` int(11) DEFAULT NULL,
  `drawer8` int(11) DEFAULT NULL,
  `drawer9` int(11) DEFAULT NULL,
  `drawer10` int(11) DEFAULT NULL,
  PRIMARY KEY (`seq`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of drawers
-- ----------------------------
INSERT INTO `drawers` VALUES ('1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1');

-- ----------------------------
-- Table structure for `settingtem`
-- ----------------------------
DROP TABLE IF EXISTS `settingtem`;
CREATE TABLE `settingtem` (
  `settingId` int(11) NOT NULL AUTO_INCREMENT,
  `settingTemNum` int(11) DEFAULT NULL,
  PRIMARY KEY (`settingId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of settingtem
-- ----------------------------
INSERT INTO `settingtem` VALUES ('1', '20');

-- ----------------------------
-- Table structure for `sys_param`
-- ----------------------------
DROP TABLE IF EXISTS `sys_param`;
CREATE TABLE `sys_param` (
  `sysParamId` int(11) NOT NULL AUTO_INCREMENT,
  `deviceCoding` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `deviceAddr` int(11) DEFAULT NULL,
  `stateUpInterval` int(11) DEFAULT NULL,
  `startOutTime` int(11) DEFAULT NULL,
  `settingTem` int(11) DEFAULT NULL,
  `TemCBias` int(11) DEFAULT NULL,
  PRIMARY KEY (`sysParamId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of sys_param
-- ----------------------------
INSERT INTO `sys_param` VALUES ('1', '68736253954', '2', '2', '2', '20', '1');
